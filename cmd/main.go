package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/morganadoche/contact"
	"go.mongodb.org/mongo-driver/bson"
)

var mh *contact.MongoHandler

/*
	create a chi router to register routes
*/
//retisterRoutes registers the routes for the router
func registerRoutes() http.Handler {
	r := chi.NewRouter()

	//chi middleware
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Testing mongo with golang. send post request on /contacts to add a new contact"))
		w.Write([]byte("params: firstname, lastname, email"))
	})
	r.Route("/contacts", func(r chi.Router) {
		r.Get("/", getAllContacts)
		r.Post("/", addContact)
	})
	return r
}

/*
create the http route handler functions to handle the routes for each request
****************************************************************************
*/

//getAllContacts fetches all contacts from the mongodb using the mongoHanlder and resposnds to the client using the json encoder
func getAllContacts(w http.ResponseWriter, r *http.Request) {
	contacts, _ := mh.Get(bson.M{})
	json.NewEncoder(w).Encode(contacts)
}

//addContact adds a new entry into the collection using the mongohandler
func addContact(w http.ResponseWriter, r *http.Request) {
	var contact contact.Contact
	json.NewDecoder(r.Body).Decode(&contact)
	res, _, err := mh.AddOne(&contact)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	fmt.Println()
	fmt.Println(res)
	fmt.Println()
	w.Write([]byte("contact created successfully"))
	w.WriteHeader(201)
}

func main() {
	mongoConnection := "mongodb://localhost:27017"
	mh, _ = contact.NewHandler(mongoConnection)
	r := registerRoutes()
	log.Fatal(http.ListenAndServe(":3001", r))
}
