package contact

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/*
	create a database string to hold the default database name
	create a collection name to hold the collection to be used
*/

//DefaultDatabase is a string type that holds the database name
const DefaultDatabase = "contactstore"

//CollectionName holds the collection to be initiallized
const CollectionName = "contact"

//MongoHandler is a struct type that holds the connection parameters
type MongoHandler struct {
	client   *mongo.Client
	database string
}

/*
	create a MongoHandler constructor
*/

//NewHandler is a constructor for MongoHandler to handle initiation of mongo
func NewHandler(address string) (mh *MongoHandler, cancelfunc context.CancelFunc) {
	ctx, cancelfunc := context.WithTimeout(context.Background(), 10*time.Second)
	cl, err := mongo.Connect(ctx, options.Client().ApplyURI(address))
	if err == nil {
		mh = &MongoHandler{
			client:   cl,
			database: DefaultDatabase,
		}
	}
	return mh, cancelfunc
}

//Get method is a MongoHandler method to get param from database
func (mh *MongoHandler) Get(filter interface{}) (contacts []*Contact, cancelfunc context.CancelFunc) {
	collection := mh.client.Database(mh.database).Collection(CollectionName)
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)

	cur, err := collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}

	defer cur.Close(ctx)

	for cur.Next(ctx) {
		contact := &Contact{}
		err = cur.Decode(contact)
		if err != nil {
			log.Fatal(err)
		}
		contacts = append(contacts, contact)
	}

	return
}

//GetOne is a mongohandler method that gets one contact from the mongo collection, return
func (mh *MongoHandler) GetOne(c *Contact, filter interface{}) (contact Contact, cancelfunc context.CancelFunc, err error) {
	collection := mh.client.Database(mh.database).Collection(CollectionName)
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	err = collection.FindOne(ctx, filter).Decode(c)
	return
}

//AddOne is a mongohandler method that adds one item to the collection
func (mh *MongoHandler) AddOne(c *Contact) (result *mongo.InsertOneResult, cancelfunc context.CancelFunc, err error) {
	collection := mh.client.Database(mh.database).Collection(CollectionName)
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	result, err = collection.InsertOne(ctx, c)
	return
}


